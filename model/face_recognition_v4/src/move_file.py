import os
import shutil
from tqdm import tqdm

path_src = r"C:\Users\Khoa\Desktop\BML_Face_Recognition-master\face_recognition_v4\dataset_mask_nomask"
path_des = r"C:\Users\Khoa\Desktop\BML_Face_Recognition-master\face_recognition_v4\dataset"

for folder_name in os.listdir(path_src):
    path_foder_src = os.path.join(path_src, folder_name)
    path_folder_des = os.path.join(path_des, folder_name)
    os.mkdir(path_folder_des)
    for subfolder_name in os.listdir(path_foder_src):
        path_subfolder = os.path.join(path_foder_src, subfolder_name)
        for fn in tqdm(os.listdir(path_subfolder)):
            path_file = os.path.join(path_subfolder,fn)
            shutil.copy(path_file,os.path.join(path_folder_des,fn))
